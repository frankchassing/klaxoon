import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Injector, NgModule} from '@angular/core';
import {HttpClientModule, HttpHandler} from '@angular/common/http';
import {AppComponent} from './app.component';
import {UpdateBookmarkDialogComponent} from './update-bookmark-dialog/update-bookmark-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlickrService} from './shared/services/flickr.service';
import {HttpService, httpServiceFactory} from './shared/services/http.service';

import {VimeoService} from './shared/services/vimeo.service';
import {KeywordsComponent} from './shared/components/keywords/keywords.component';
import {NotificationsService} from './shared/services/notifications.service';
import {NotificationsComponent} from './shared/components/notifications/notifications.component';

import {DropdownModule, ProgressSpinnerModule, GrowlModule, ButtonModule, DialogModule, SelectButtonModule} from 'primeng/primeng';
import {DataViewModule} from 'primeng/dataview';
import {MessageService} from 'primeng/components/common/messageservice';

@NgModule({
  declarations: [
    AppComponent,
    UpdateBookmarkDialogComponent,
    KeywordsComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    ButtonModule,
    DialogModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    SelectButtonModule,
    HttpClientModule,
    DataViewModule,
    GrowlModule,
    DropdownModule,
    ProgressSpinnerModule
  ],
  providers: [
    {
      provide: HttpService,
      useFactory: httpServiceFactory,
      deps: [HttpHandler, Injector]
    },
    FlickrService,
    VimeoService,
    NotificationsService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
