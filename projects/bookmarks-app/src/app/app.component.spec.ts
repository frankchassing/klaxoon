import {TestBed, async, ComponentFixture, getTestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {AppModule} from './app.module';
import {FlickrService} from './shared/services/flickr.service';
import {VimeoService} from './shared/services/vimeo.service';
import {of, throwError} from 'rxjs/index';
import {NotificationsService} from './shared/services/notifications.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let flickrService: FlickrService;
  let vimeoService: VimeoService;
  let notificationsService: NotificationsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    flickrService = getTestBed().get(FlickrService);
    vimeoService = getTestBed().get(VimeoService);
    notificationsService = getTestBed().get(NotificationsService);
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  describe('Manage bookmarks', () => {
    it('should update an existing bookmark', async(() => {
      component.bookmarks = [{
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 363,
        height: 2160,
        id: '261794608',
        title: 'Undisturbed Norway - a timelapse adventure',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      }];

      component.updateBookmarks({
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 400,
        height: 3000,
        id: '261794608',
        title: 'Title1',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      });

      expect(component.bookmarks[0]).toEqual({
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 400,
        height: 3000,
        id: '261794608',
        title: 'Title1',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      });
    }));

    it('should delete an existing bookmark', async(() => {
      component.bookmarks = [{
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 363,
        height: 2160,
        id: '261794608',
        title: 'Undisturbed Norway - a timelapse adventure',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      }];

      component.deleteBookmark({
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 363,
        height: 2160,
        id: '261794608',
        title: 'Undisturbed Norway - a timelapse adventure',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      });

      expect(component.bookmarks).toEqual([]);
    }));

    it('should add a flickr bookmark', async(() => {
      component.bookmarks = [];
      component.urlType = 'Flickr';
      component.urlToAdd = 'https://www.flickr.com/photos/primerx24/7702561586/in/photolist';
      component.urlIdToAdd = '7702561586';

      spyOn(flickrService, 'getPhotoInfos').and.returnValue(
        of({
          photo: {
            id: '7702561586',
            title: {
              _content: 'title'
            },
            owner: {
              username: 'authorName'
            },
          }
        })
      );
      spyOn(flickrService, 'getPhotoSize').and.returnValue(
        of({
          sizes: {
            size: [{
              label: 'Original',
              width: '1920',
              height: '1080'
            }]
          }
        })
      );
      spyOn(notificationsService, 'success');
      component.addBookmark();

      expect(flickrService.getPhotoInfos).toHaveBeenCalledWith('7702561586');
      expect(flickrService.getPhotoSize).toHaveBeenCalledWith('7702561586');

      expect(component.bookmarks[0].id).toEqual('7702561586');
      expect(component.bookmarks[0].url).toEqual('https://www.flickr.com/photos/primerx24/7702561586/in/photolist');
      expect(component.bookmarks[0].title).toEqual('title');
      expect(component.bookmarks[0].authorName).toEqual('authorName');
      expect(component.bookmarks[0].urlType).toEqual('Flickr');
      expect(component.bookmarks[0].width).toEqual('1920');
      expect(component.bookmarks[0].height).toEqual('1080');
      expect(component.bookmarks[0].keywords).toEqual([]);

      expect(notificationsService.success).toHaveBeenCalledWith('Lien ajouté !', 'Le bookmark a bien été créé');

    }));

    it('should add a vimeo bookmark', async(() => {
      component.bookmarks = [];
      component.urlType = 'Vimeo';
      component.urlToAdd = 'https://vimeo.com/248772987';
      component.urlIdToAdd = '248772987';

      spyOn(vimeoService, 'getVideoInfos').and.returnValue(
        of({
          name: 'videoName',
          user: {
            name: 'frank'
          },
          width: '1920',
          height: '1080',
          duration: '345'
        })
      );
      spyOn(notificationsService, 'success');
      component.addBookmark();

      expect(vimeoService.getVideoInfos).toHaveBeenCalledWith('248772987');

      expect(component.bookmarks[0].id).toEqual('248772987');
      expect(component.bookmarks[0].url).toEqual('https://vimeo.com/248772987');
      expect(component.bookmarks[0].title).toEqual('videoName');
      expect(component.bookmarks[0].authorName).toEqual('frank');
      expect(component.bookmarks[0].urlType).toEqual('Vimeo');
      expect(component.bookmarks[0].width).toEqual('1920');
      expect(component.bookmarks[0].height).toEqual('1080');
      expect(component.bookmarks[0].duration).toEqual('345');
      expect(component.bookmarks[0].keywords).toEqual([]);

      expect(notificationsService.success).toHaveBeenCalledWith('Lien ajouté !', 'Le bookmark a bien été créé');
    }));

    it('should add a flickr bookmark - Error Flickr API', async(() => {
      component.bookmarks = [];
      component.urlType = 'Flickr';
      component.urlToAdd = 'https://www.flickr.com/photos/primerx24/7702561586/in/photolist';

      spyOn(flickrService, 'getPhotoInfos').and.returnValue(
        of({
          error: '500'
        })
      );

      spyOn(notificationsService, 'error');
      component.addBookmark();

      expect(notificationsService.error).toHaveBeenCalledWith('Erreur', 'Une erreur est survenue durant la récupération des informations de la photo');
    }));

    it('should add a vimeo bookmark - Error Vimeo API 404', async(() => {
      component.bookmarks = [];
      component.urlType = 'Vimeo';
      component.urlToAdd = 'https://vimeo.com/248772987';

      spyOn(vimeoService, 'getVideoInfos').and.returnValue(
        throwError({
          status: 404
        })
      );

      spyOn(notificationsService, 'error');
      component.addBookmark();

      expect(notificationsService.error).toHaveBeenCalledWith('Informations introuvables', 'La vidéo n\'a pas été trouvée');
    }));

    it('should add a vimeo bookmark - Error Vimeo API 500', async(() => {
      component.bookmarks = [];
      component.urlType = 'Vimeo';
      component.urlToAdd = 'https://vimeo.com/248772987';


      spyOn(vimeoService, 'getVideoInfos').and.returnValue(
        throwError({
          status: 500
        })
      );
      spyOn(notificationsService, 'error');
      component.addBookmark();

      expect(notificationsService.error).toHaveBeenCalledWith('Erreur 500', 'Une erreur est survenue durant la récupération des informations de la vidéo');
    }));

    it('should add a bookmark - Error wrong url type', async(() => {
      component.bookmarks = [];
      component.urlType = 'Wrong';
      component.urlToAdd = 'https://vimeo.com/248772987';

      spyOn(notificationsService, 'error');
      component.addBookmark();

      expect(notificationsService.error).toHaveBeenCalledWith('Erreur', 'Impossible de déterminer le type du lien');
    }));
  });

  describe('Check URL validity', () => {
    it('should check if URL already exists', async(() => {
      component.bookmarks = [{
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 363,
        height: 2160,
        id: '261794608',
        title: 'Undisturbed Norway - a timelapse adventure',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      }];
      component.urlType = 'Vimeo';
      component.urlToAdd = 'https://vimeo.com/261794608';

      component.checkURLValidity();

      expect(component.errorURLValidity).toEqual('Un bookmark avec la même URL existe déjà');
    }));

    it('should check if the URL match a flickr or vimeo URL - ERROR', async(() => {
      component.bookmarks = [];
      component.urlType = 'Flickr';
      component.urlToAdd = 'https://www.flikr.com/photos/primerx24/7702561586/in/photolist';

      component.checkURLValidity();

      expect(component.errorURLValidity).toEqual('Le lien ne correspond ni à un lien photo Flickr, ni à un lien vidéo Vimeo');
    }));

    it('should check if the URL match a flickr\'s photo - OK', async(() => {
      component.bookmarks = [];
      component.urlType = '';
      component.urlToAdd = 'https://www.flickr.com/photos/primerx24/7702561586/in/photolist';

      component.checkURLValidity();

      expect(component.urlIdToAdd).toEqual('7702561586');
      expect(component.urlType).toEqual('Flickr');
    }));

    it('should check if the URL match a vimeo\'s video - OK', async(() => {
      component.bookmarks = [];
      component.urlType = '';
      component.urlToAdd = 'https://vimeo.com/261794608';

      component.checkURLValidity();

      expect(component.urlIdToAdd).toEqual('261794608');
      expect(component.urlType).toEqual('Vimeo');
    }));

  });

  describe('Manage update dialog', () => {
    it('should update display variable', async(() => {
      component.updateDisplayDialog(true);
      expect(component.displayDialogUpdate).toBeTruthy();
    }));

    it('should show update dialog', async(() => {
      const bookmark = {
        addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
        authorName: 'Casper Rolsted',
        duration: 363,
        height: 2160,
        id: '261794608',
        title: 'Undisturbed Norway - a timelapse adventure',
        url: 'https://vimeo.com/261794608',
        urlType: 'Vimeo',
        width: 3840,
        keywords: ['frank1', 'frank2']
      };
      component.showDialogUpdate(bookmark);
      expect(component.displayDialogUpdate).toBeTruthy();
      expect(component.bookmarkToUpdate).toEqual(bookmark);
    }));
  });

  it('should update keywords', async(() => {
    component.keywords = [];
    component.updateKeywords(['key1', 'key2']);
    expect(component.keywords).toEqual(['key1', 'key2']);
  }));

  it('should sort bookmarks', async(() => {
    component.bookmarks = [{
      title: 'ABC',
      addedDate: new Date('Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
    },
      {
        title: 'DEF',
        addedDate: new Date('Mon Apr 02 2017 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      }];
    component.onSortChange({value: '>addedDate'});

    expect(component.bookmarks).toEqual([
      {
        title: 'DEF',
        addedDate: new Date('Mon Apr 02 2017 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      },
      {
        title: 'ABC',
        addedDate: new Date('Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      }]);

    component.onSortChange({value: '<title'});

    expect(component.bookmarks).toEqual([
      {
        title: 'ABC',
        addedDate: new Date('Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      },
      {
        title: 'DEF',
        addedDate: new Date('Mon Apr 02 2017 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      }]);

    component.onSortChange({value: '>title'});

    expect(component.bookmarks).toEqual([
      {
        title: 'DEF',
        addedDate: new Date('Mon Apr 02 2017 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      },
      {
        title: 'ABC',
        addedDate: new Date('Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      }]);

    component.onSortChange({value: '<addedDate'});

    expect(component.bookmarks).toEqual([
      {
        title: 'ABC',
        addedDate: new Date('Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      },
      {
        title: 'DEF',
        addedDate: new Date('Mon Apr 02 2017 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))')
      }]);
  }));

});
