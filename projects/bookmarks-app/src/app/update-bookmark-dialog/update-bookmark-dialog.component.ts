import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

@Component({
  selector: 'msp-update-bookmark-dialog',
  templateUrl: './update-bookmark-dialog.component.html',
  styleUrls: ['./update-bookmark-dialog.component.scss']
})
export class UpdateBookmarkDialogComponent implements OnInit {
  _display: boolean;
  @Input() bookmarkToUpdate;

  @Input()
  public get display(): boolean {
    return this._display;
  }

  public set display(value: boolean) {
    this._display = value;
    this.displayChange.emit(value);
  }

  @Output() displayChange: EventEmitter<boolean> = new EventEmitter();
  @Output() bookmarksChange: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  /**
   * Emit the display change to hide the update dialog
   */
  cancel() {
    this.displayChange.emit(false);
  }

  /**
   * Emit the new updated bookmark and emit the display change to hide the update dialog
   */
  onSubmit() {
    this.bookmarksChange.emit(this.bookmarkToUpdate);
    this.displayChange.emit(false);
  }

  /**
   * Update the keywords with the given ones
   * @param keywords
   */
  updateKeywords(keywords) {
    this.bookmarkToUpdate.keywords = keywords;
  }

}
