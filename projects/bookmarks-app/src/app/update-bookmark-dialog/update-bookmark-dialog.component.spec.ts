import {TestBed, async, ComponentFixture, getTestBed} from '@angular/core/testing';
import {AppModule} from '../app.module';
import {UpdateBookmarkDialogComponent} from './update-bookmark-dialog.component';
import {FormBuilder} from '@angular/forms';

describe('KeywordsComponent', () => {
  let component: UpdateBookmarkDialogComponent;
  let fixture: ComponentFixture<UpdateBookmarkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBookmarkDialogComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should cancel the update dialog', async(() => {
    spyOn(component.displayChange, 'emit');
    component.cancel();
    expect(component.displayChange.emit).toHaveBeenCalledWith(false);
  }));

  it('should submit the update dialog', async(() => {
    spyOn(component.bookmarksChange, 'emit');
    spyOn(component.displayChange, 'emit');
    component.bookmarkToUpdate = {
      addedDate: 'Mon Apr 02 2018 10:03:14 GMT+0200 (Paris, Madrid (heure d’été))',
      authorName: 'Casper Rolsted',
      duration: 363,
      height: 2160,
      id: '261794608',
      title: 'Undisturbed Norway - a timelapse adventure',
      url: 'https://vimeo.com/261794608',
      urlType: 'Vimeo',
      width: 3840,
      keywords: ['frank1', 'frank2']
    };
    component.onSubmit();
    expect(component.bookmarksChange.emit).toHaveBeenCalledWith(component.bookmarkToUpdate);
    expect(component.displayChange.emit).toHaveBeenCalledWith(false);
  }));

  it('should update the keywords', async(() => {
    component.bookmarkToUpdate = {};
    component.updateKeywords(['frank1', 'frank2']);
    expect(component.bookmarkToUpdate.keywords).toEqual(['frank1', 'frank2']);
  }));
});
