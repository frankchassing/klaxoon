import {Injectable} from '@angular/core';
import {CommonService} from './common.service';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/index';
import {HttpParams} from '@angular/common/http';
import {FlickrPhoto, FlickrSizes} from '../models/photo-flickr';
import {Constants} from '../constants';

@Injectable()
export class FlickrService extends CommonService {

  constructor(private http: HttpService) {
    super();
  }

  /**
   * Get the informations of a photo from the Flickr API
   * @param {string} photoId
   * @returns {Observable<FlickrPhoto>}
   */
  getPhotoInfos(photoId: string): Observable<FlickrPhoto> {
    const params = new HttpParams()
      .set('method', 'flickr.photos.getInfo')
      .set('api_key', Constants.FLICKR_API_KEY)
      .set('photo_id', photoId)
      .set('format', 'json')
      .set('nojsoncallback', '1');
    return this.http.get(`${Constants.FLICKR_API_BASE_URL}`, {params: params});
  }

  /**
   * Get the sizes informations of a photo from the Flickr API
   * @param {string} photoId
   * @returns {Observable<FlickrSizes>}
   */
  getPhotoSize(photoId: string): Observable<FlickrSizes> {
    const params = new HttpParams()
      .set('method', 'flickr.photos.getSizes')
      .set('api_key', Constants.FLICKR_API_KEY)
      .set('photo_id', photoId)
      .set('format', 'json')
      .set('nojsoncallback', '1');
    return this.http.get(`${Constants.FLICKR_API_BASE_URL}`, {params: params});
  }
}
