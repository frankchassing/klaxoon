import {TestBed, async, inject} from '@angular/core/testing';
import {VimeoService} from './vimeo.service';
import {of} from 'rxjs/index';
import {HttpService} from './http.service';
import {HttpHeaders, HttpParams} from '@angular/common/http';

describe('VimeoService', () => {
  let service: VimeoService;
  const httpService = jasmine.createSpyObj('HttpService', {
    get: of({}),
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        VimeoService,
        {provide: HttpService, useValue: httpService}
      ],
    });
  }));

  beforeEach(inject([VimeoService], (_service: VimeoService) => {
    service = _service;
  }));

  describe('Call all flickr service functions', () => {
    it('should call getPhotoInfos()', async(() => {
      const headers = new HttpHeaders()
        .set('Authorization', 'Bearer f682811c10dcaecd71bd9a88e9e0bef1');
      service.getVideoInfos('261794608');
      expect(httpService.get).toHaveBeenCalled();
      expect(httpService.get.calls.mostRecent().args[0]).toEqual('https://api.vimeo.com/videos/261794608');
      expect(httpService.get.calls.mostRecent().args[1].headers.toString()).toEqual(headers.toString());
    }));
  });

});
