import {Injectable} from '@angular/core';
import {CommonService} from './common.service';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/index';
import {HttpHeaders} from '@angular/common/http';
import {VimeoVideo} from '../models/video-vimeo';
import {Constants} from '../constants';

@Injectable()
export class VimeoService extends CommonService {

  constructor(private http: HttpService) {
    super();
  }

  /**
   * Get the informations of a video from the Vimeo API
   * @param {string} videoId
   * @returns {Observable<VimeoVideo>}
   */
  getVideoInfos(videoId: string): Observable<VimeoVideo> {
    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${Constants.VIMEO_TOKEN}`);
    return this.http.get(`${Constants.VIMEO_API_BASE_URL}videos/${videoId}`, {headers : headers});
  }

}
