import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

type Severities = 'success' | 'info' | 'warn' | 'error';

@Injectable()
export class NotificationsService {
  notificationChange: Subject<Object> = new Subject<Object>();

  notify(severity: Severities, summary: string, detail: string) {
    this.notificationChange.next({ severity, summary, detail });
  }

  error(summary: string, detail: string) {
    this.notify('error', summary, detail);
  }

  success(summary: string, detail: string) {
    this.notify('success', summary, detail);
  }

  warn(summary: string, detail: string) {
    this.notify('warn', summary, detail);
  }
}
