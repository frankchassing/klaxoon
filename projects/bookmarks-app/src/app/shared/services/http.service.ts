import {HttpClient, HttpHandler} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';

import { Observable, Observer, Subscription, of} from 'rxjs/index';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class HttpService extends HttpClient {
  public pendingRequests = 0;
  public blockingRequests = 0;
  private emitter: EventEmitter<any> = new EventEmitter();

  constructor(private httpHandler: HttpHandler) {
    super(httpHandler);
  }

  private static catchExpectedError(err): Observable<any> {
    if (err.error) {
      if (err.error.code) {
        // this is an expected error from backend, with associated code, handle it as normal code even if http status is 5xx or 4xx
        return of(err.error);
      }
      throw err;
    }
  }

  private onRequest(blockUI: boolean = true): void {
    if (blockUI) {
      this.blockingRequests++;
    }
    this.pendingRequests++;
    this.emitter.emit({
      action: 'start',
      count: this.pendingRequests,
    });
  }

  onRequestEnd(blockUI: boolean = true): void {
    if (blockUI) {
      this.blockingRequests--;
    }
    this.pendingRequests--;
    this.emitter.emit({
      action: 'end',
      count: this.pendingRequests,
    });
  }

  onError(err): void {
    this.emitter.emit({
      action: 'error',
      error: err,
    });
  }


  get<T>(url: string, options?): Observable<T> {
    const blockUI = !options || options.blockUI === undefined || options.blockUI;

    return Observable.create((observer: Observer<T>) => {
      this.onRequest(blockUI);
      const subscribe: Subscription = super.get((url), options)
        .pipe(
          catchError(HttpService.catchExpectedError),
          map(res => {
            this.onRequestEnd(blockUI);
            return res;
          }))
        .subscribe(
          res => {
            subscribe.unsubscribe();
            observer.next(res);
            observer.complete();
          },
          err => {
            this.onRequestEnd(blockUI);
            this.onError(err);
            subscribe.unsubscribe();
            observer.error(err);
            observer.complete();
          });
    });
  }

  post<T>(url: string, body?: any, options?): Observable<T> {
    const blockUI = !options || options.blockUI === undefined || options.blockUI;
    return Observable.create((observer: Observer<T>) => {
      this.onRequest(blockUI);
      const subscribe: Subscription = super.post(url, body, options)
        .pipe(
          catchError(HttpService.catchExpectedError),
          map(res => {
            this.onRequestEnd(blockUI);
            return res;
          }))
        .subscribe(
          res => {
            subscribe.unsubscribe();
            observer.next(res);
            observer.complete();
          },
          err => {
            this.onRequestEnd(blockUI);
            subscribe.unsubscribe();
            observer.error(err);
            observer.complete();
          });
    });
  }

  put<T>(url: string, body: any, options?): Observable<T> {
    const blockUI = !options || options.blockUI === undefined || options.blockUI;
    return Observable.create((observer: Observer<T>) => {
      this.onRequest(blockUI);
      const subscribe: Subscription = super.put(url, body, options)
        .pipe(
          catchError(HttpService.catchExpectedError),
          map(res => {
            this.onRequestEnd(blockUI);
            return res;
          }))
        .subscribe(
          res => {
            subscribe.unsubscribe();
            observer.next(res);
            observer.complete();
          },
          err => {
            this.onRequestEnd(blockUI);
            subscribe.unsubscribe();
            observer.error(err);
            observer.complete();
          });
    });
  }

  del<T>(url: string, options?): Observable<T> {
    const blockUI = !options || options.blockUI === undefined || options.blockUI;
    return Observable.create((observer: Observer<T>) => {
      this.onRequest(blockUI);
      const subscribe: Subscription = super.delete(url, options)
        .pipe(
          catchError(HttpService.catchExpectedError),
          map(res => {
            this.onRequestEnd(blockUI);
            return res;
          }))
        .subscribe(
          res => {
            subscribe.unsubscribe();
            observer.next(res);
            observer.complete();
          },
          err => {
            this.onRequestEnd(blockUI);
            subscribe.unsubscribe();
            observer.error(err);
            observer.complete();
          });
    });
  }
}

export function httpServiceFactory(httpHandler: HttpHandler) {
  return new HttpService(httpHandler);
}
