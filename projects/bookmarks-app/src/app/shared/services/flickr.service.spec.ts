import {TestBed, async, inject} from '@angular/core/testing';
import {FlickrService} from './flickr.service';
import {of} from 'rxjs/index';
import {HttpService} from './http.service';
import {HttpParams} from '@angular/common/http';

describe('FlickrService', () => {
  let service: FlickrService;
  const httpService = jasmine.createSpyObj('HttpService', {
    get: of({}),
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        FlickrService,
        {provide: HttpService, useValue: httpService}
      ],
    });
  }));

  beforeEach(inject([FlickrService], (_service: FlickrService) => {
    service = _service;
  }));

  describe('Call all flickr service functions', () => {
    it('should call getPhotoInfos()', async(() => {
      service.getPhotoInfos('261794608');
      const params = new HttpParams()
        .set('method', 'flickr.photos.getInfo')
        .set('api_key', 'dc47d01de89eca423ee6eb9b008974e0')
        .set('photo_id', '261794608')
        .set('format', 'json')
        .set('nojsoncallback', '1');
      expect(httpService.get).toHaveBeenCalled();
      expect(httpService.get.calls.mostRecent().args[0]).toEqual('https://api.flickr.com/services/rest/');
      expect(httpService.get.calls.mostRecent().args[1].params.toString()).toEqual(params.toString());
    }));

    it('should call getPhotoSize()', async(() => {
      service.getPhotoSize('261794608');
      const params = new HttpParams()
        .set('method', 'flickr.photos.getSizes')
        .set('api_key', 'dc47d01de89eca423ee6eb9b008974e0')
        .set('photo_id', '261794608')
        .set('format', 'json')
        .set('nojsoncallback', '1');
      expect(httpService.get).toHaveBeenCalled();
      expect(httpService.get.calls.mostRecent().args[0]).toEqual('https://api.flickr.com/services/rest/');
      expect(httpService.get.calls.mostRecent().args[1].params.toString()).toEqual(params.toString());
    }));
  });

});
