export interface FlickrPhoto {
  photo: Photo;
}

export interface Photo {
  id: number;
  owner: Owner;
  title: Title;
}

export interface Owner {
  username: string;
}

export interface Title {
  _content: string;
}

export interface FlickrSizes {
  sizes: Sizes;
}

export interface Sizes {
  size: Size[];
}

export interface Size {
  label: string;
  width: string;
  height: string;
}
