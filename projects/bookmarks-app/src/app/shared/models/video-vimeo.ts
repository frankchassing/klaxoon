export interface VimeoVideo {
  name: string;
  user: User;
  width: string;
  height: string;
  duration: string;
}

export interface User {
  name: string;
}
