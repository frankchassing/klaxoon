import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

@Component({
  selector: 'msp-keywords',
  templateUrl: './keywords.component.html',
  styleUrls: ['./keywords.component.scss'],
})
export class KeywordsComponent implements OnInit {

  private _keywords: any;
  keywordToAdd: string;

  @Input()
  public get keywords(): any {
    return this._keywords;
  }

  public set keywords(value: any) {
    this._keywords = value;
  }

  @Input() theme: string;

  @Output() keywordsChange: EventEmitter<boolean> = new EventEmitter();

  constructor() {}
  ngOnInit() {}

  /**
   * Add the current keyword to the keyword list and then emit the new keywords
   */
  addKeyword() {
    this.keywords = [...this.keywords, this.keywordToAdd];
    this.keywordsChange.emit(this.keywords);
    this.keywordToAdd = undefined;
  }

  /**
   * Delete the given keyword to the keyword list and then emit the new keywords
   */
  deleteKeyword(keyword) {
    this.keywords.splice(this.keywords.indexOf(keyword), 1);
    this.keywordsChange.emit(this.keywords);
  }

}
