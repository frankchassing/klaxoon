import {TestBed, async, ComponentFixture, getTestBed} from '@angular/core/testing';
import {AppModule} from '../../../app.module';
import {KeywordsComponent} from './keywords.component';

describe('KeywordsComponent', () => {
  let component: KeywordsComponent;
  let fixture: ComponentFixture<KeywordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordsComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', async(() => {
    expect(component).toBeTruthy();
  }));

  describe('Manage keywords', () => {
    it('should add a keyword', async(() => {
      component.keywordToAdd = 'keyToAdd';
      component.keywords = [];

      component.addKeyword();

      expect(component.keywords).toEqual(['keyToAdd']);
    }));

    it('should delete a keyword', async(() => {
      component.keywords = ['keyToDelete'];

      component.deleteKeyword('keyToDelete');

      expect(component.keywords).toEqual([]);
    }));
  });

});
