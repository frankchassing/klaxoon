import {Component} from '@angular/core';
import {FlickrService} from './shared/services/flickr.service';
import {VimeoService} from './shared/services/vimeo.service';
import {NotificationsService} from './shared/services/notifications.service';
import {style, animate, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0, transform: 'translateY(-50%)' }),
        animate(300, style({opacity: 1, transform: 'translateY(0)'}))

      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(300, style({opacity: 0, transform: 'translateY(-50%)'}))
      ])
    ])
  ]
})
export class AppComponent {
  bookmarks: any = [];
  keywords: string[] = [];
  urlToAdd: string;
  urlIdToAdd: string;
  urlType: string;
  errorURLValidity: string;
  sortKey: string;
  filterType = '';
  requestIsPending = false;
  bookmarkToUpdate = {};
  displayDialogUpdate = false;

  sortOptions = [
    {label: 'Date d\'ajout décroissante', value: '<addedDate'},
    {label: 'Date d\'ajout croissante', value: '>addedDate'},
    {label: 'Titre A-Z', value: '<title'},
    {label: 'Titre Z-A', value: '>title'},
  ];

  filterOptions = [
      {label: 'Tous', value: '', icon: 'fa fa-link'},
      {label: 'Flickr', value: 'Flickr', icon: 'fa fa-flickr'},
      {label: 'Vimeo', value: 'Vimeo', icon: 'fa fa-vimeo'}
  ];

  constructor(private flickrService: FlickrService, private vimeoService: VimeoService,
              private notificationsService: NotificationsService) {
  }

  /**
   * Add a bookmark in the bookmark list according to the given URL
   */
  addBookmark() {
    if (this.urlType === 'Flickr') {
      this.requestIsPending = true;
      this.flickrService.getPhotoInfos(this.urlIdToAdd).subscribe(data => {
        if (data.photo) {
          this.flickrService.getPhotoSize(this.urlIdToAdd).subscribe(dataSizes => {
              const {width = 'NC', height = 'NC'} = dataSizes.sizes ? dataSizes.sizes.size.find(s => s.label === 'Original') || {} : {};
              this.bookmarks = [{
                id: this.urlIdToAdd,
                url: this.urlToAdd,
                title: data.photo.title ? data.photo.title._content : 'NC',
                authorName: data.photo.owner ? data.photo.owner.username : 'NC',
                addedDate: new Date(),
                urlType: this.urlType,
                width: width,
                height: height,
                keywords: this.keywords
              }, ...this.bookmarks];
              this.notificationsService.success('Lien ajouté !', 'Le bookmark a bien été créé');
              this.resetVars();
          });
        } else {
          this.notificationsService.error('Erreur', 'Une erreur est survenue durant la récupération des informations de la photo');
          this.requestIsPending = false;
        }
      });
    } else if (this.urlType === 'Vimeo') {
      this.requestIsPending = true;
      this.vimeoService.getVideoInfos(this.urlIdToAdd).subscribe(video => {
          this.bookmarks = [{
            id: this.urlIdToAdd,
            url: this.urlToAdd,
            title: video.name,
            authorName: video.user ? video.user.name : 'Unknown',
            addedDate: new Date(),
            width: video.width,
            height: video.height,
            duration: video.duration,
            urlType: this.urlType,
            keywords: this.keywords
          }, ...this.bookmarks];
          this.notificationsService.success('Lien ajouté !', 'Le bookmark a bien été créé');
          this.resetVars();
      }, error => {
         error.status === 404 ?
           this.notificationsService.error('Informations introuvables', 'La vidéo n\'a pas été trouvée') :
           this.notificationsService.error(`Erreur ${error.status}`, 'Une erreur est survenue durant la récupération des informations de la vidéo');
        this.requestIsPending = false;
      });
    } else {
      this.notificationsService.error('Erreur', 'Impossible de déterminer le type du lien');
    }
  }

  /**
   * Reset the 'url to add' inputs
   */
  resetVars() {
    this.urlToAdd = this.urlIdToAdd = undefined;
    this.keywords = [];
    this.requestIsPending = false;
    this.onSortChange({value: this.sortKey});
  }

  /**
   * Update the bookmark given in the bookmark list
   * @param bookmark
   */
  updateBookmarks(bookmark: any) {
    this.bookmarks.splice(this.bookmarks.findIndex(element => element.url === bookmark.url), 1, bookmark);
  }

  /**
   * Show/Hide the update dialog according to the given parameter
   * @param {boolean} display
   */
  updateDisplayDialog(display: boolean) {
    this.displayDialogUpdate = display;
  }

  /**
   * Show the update dialog for the given bookmark
   * @param bookmarkToUpdate
   */
  showDialogUpdate(bookmarkToUpdate) {
    this.displayDialogUpdate = true;
    this.bookmarkToUpdate = bookmarkToUpdate;
  }

  /**
   * Delete the given bookmark from the bookmarks list
   * @param bookmark
   */
  deleteBookmark(bookmark) {
    this.bookmarks.splice(this.bookmarks.findIndex(element => element.url === bookmark.url), 1);
    this.checkURLValidity();
  }

  /**
   * Update the keywords with the given ones
   * @param keywords
   */
  updateKeywords(keywords) {
    this.keywords = [...keywords];
  }

  /**
   * Manage the sort of the bookmarks list
   * @param event
   */
  onSortChange({value}) {
    const [first, ...next] = value;
    const sortOrder = first === '>' ? -1 : 1;

    if (next === 'addedDate') {
      this.bookmarks.sort((a, b) => (new Date(b.addedDate).getTime() - new Date(a.addedDate).getTime()) * sortOrder);
    } else if (next === 'title') {
      this.bookmarks.sort((a, b) =>  (a.title < b.title) ? -1 * sortOrder : sortOrder);
    }
    this.bookmarks = [...this.bookmarks];
  }

  /**
   * Check URL validity on change
   * The link must match Flickr or Vimeo URL
   */
  checkURLValidity() {
    this.urlIdToAdd = undefined;
    if (this.bookmarks.find(element => element.url === this.urlToAdd)) {
      this.errorURLValidity = 'Un bookmark avec la même URL existe déjà';
    } else {
      const matchFlickr = /(?:^|\s)https:\/\/www.flickr.com\/photos\/.*\/([0-9]*)\/.*(?:\s|$)/g.exec(this.urlToAdd);
      if (!matchFlickr) {
        const matchVimeo = /(?:^|\s)https:\/\/vimeo.com\/([0-9]*)\/?.*(?:\s|$)/g.exec(this.urlToAdd);
        if (!matchVimeo) {
          this.errorURLValidity = 'Le lien ne correspond ni à un lien photo Flickr, ni à un lien vidéo Vimeo';
        } else {
          this.urlIdToAdd = matchVimeo[1];
          this.urlType = 'Vimeo';
        }
      } else {
        this.urlIdToAdd = matchFlickr[1];
        this.urlType = 'Flickr';
      }
    }
  }
}
